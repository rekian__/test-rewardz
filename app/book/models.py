from django.db import models

from app.book_information.models import BookInformation


class StatusChoices(models.TextChoices):
    AVAILABLE = 'available', 'Available'
    BORROWED = 'borrowed', 'Borrowed'
    LOST = 'lost', 'Lost'
    BROKEN = 'broken', 'Broken'


# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, null=True)
    description = models.CharField(null=True, blank=True, max_length=255)
    content = models.TextField(null=True, blank=True)
    code = models.CharField(unique=True, max_length=32)
    book_information = models.ForeignKey(BookInformation, on_delete=models.CASCADE)
    status = models.CharField(
        max_length=25,
        choices=StatusChoices.choices,
        default=StatusChoices.AVAILABLE
    )

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    edited_at = models.DateTimeField(auto_now=True, null=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.title
