from rest_framework import serializers

from app.book_information.models import BookInformation
from app.borrowing.models import Borrowing
from app.borrowing.serializers.borrowing_item import BorrowingItemSerializer


class BorrowingSerializer(serializers.ModelSerializer):
    borrowing_items = BorrowingItemSerializer(many=True, required=False)

    class Meta:
        model = Borrowing
        fields = (
            "id", "code", "user", "created_at", "edited_at", "borrowing_items"
        )
