from django.apps import AppConfig


class BookInformationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'app.book_information'
