from django.contrib.auth.models import User
from django.db import models

from app.book.models import Book


# Create your models here.
class Borrowing(models.Model):
    code = models.CharField(unique=True, max_length=32)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    edited_at = models.DateTimeField(auto_now=True, null=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return str(self.code)

    @classmethod
    def get_list_borrowing(self):
        return self.objects.filter().order_by("-id")

    @classmethod
    def get_detail_borrowing(self, code=None):
        return self.get_list_borrowing().filter(code=code).first()


class StatusChoices(models.TextChoices):
    ACTIVE = 'active', 'Active'
    RETURNED = 'returned', 'Returned'


class BorrowingItem(models.Model):
    borrowing = models.ForeignKey(Borrowing, related_name='borrowing_items', on_delete=models.CASCADE)
    book = models.ForeignKey(Book, on_delete=models.CASCADE)
    borrow_date = models.DateField(null=True)
    return_date = models.DateField(null=True)
    is_extended = models.BooleanField(default=False)
    status = models.CharField(max_length=25, choices=StatusChoices.choices, default=StatusChoices.ACTIVE)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    edited_at = models.DateTimeField(auto_now=True, null=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"{str(self.borrowing.code)} | {str(self.book)}"

    @classmethod
    def get_list_borrowing_item(self, code, status=None):
        query_set = self.objects.filter(borrowing__code=code)
        if status:
            query_set = query_set.filter(
                status=status
            )
        return query_set.order_by("-id")

    # TODO POST_SAVE function, so everytime this table updated, the data on Book.available, Book.will_available_at also will be update
    # This kind of distributed data will speed up on query
    # def update_all_related_data(self):
