from django.db import models


# Create your models here.
class BookInformation(models.Model):
    title = models.CharField(max_length=255)
    slug = models.SlugField(max_length=255, null=True)
    description = models.CharField(null=True, blank=True, max_length=255)
    content = models.TextField(null=True, blank=True)
    code = models.CharField(unique=True, max_length=32)
    quantity = models.IntegerField()
    available = models.IntegerField(default=0)
    will_available_at = models.DateField(null=True, blank=True)
    writer = models.CharField(max_length=255)
    editor = models.CharField(max_length=255)
    publisher = models.CharField(max_length=255)
    shelf_code = models.CharField(max_length=255)
    published_date = models.DateField(null=True, blank=True)
    published_sequence = models.IntegerField()

    created_at = models.DateTimeField(auto_now_add=True, null=True)
    edited_at = models.DateTimeField(auto_now=True, null=True)
    deleted_at = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return self.title

    @classmethod
    def get_list_book(self):
        return self.objects.filter().order_by("-id")
