from rest_framework import serializers

from app.book_information.models import BookInformation


class BookInformationSerializer(serializers.ModelSerializer):

    class Meta:
        model = BookInformation
        fields = (
            "id", "title", "slug", "description", "content", "code", "quantity", "available", "will_available_at",
            "writer", "editor", "publisher", "shelf_code", "published_date", "published_sequence", "created_at",
            "edited_at",
        )
