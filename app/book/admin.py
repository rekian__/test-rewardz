from django.contrib import admin

from app.book.models import Book

# Register your models here.
admin.site.register(Book)
