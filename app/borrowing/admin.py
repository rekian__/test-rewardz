from django.contrib import admin

from app.borrowing.models import Borrowing, BorrowingItem

# Register your models here.
admin.site.register(Borrowing)
admin.site.register(BorrowingItem)
