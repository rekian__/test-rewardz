from rest_framework import viewsets, generics

from app.book_information.serializers import BookInformationSerializer
from app.borrowing.models import Borrowing, BorrowingItem
from app.borrowing.serializers.borrow import BorrowItemSerializer
from app.borrowing.serializers.borrowing import BorrowingSerializer
from app.borrowing.serializers.borrowing_item import BorrowingItemSerializer


# Create your views here.
class BorrowingViews(viewsets.ModelViewSet):
    model = Borrowing
    serializer_class = BorrowingSerializer

    def get_queryset(self):
        return Borrowing.get_list_borrowing()


# Create your views here.
class BorrowingItemsViews(viewsets.ModelViewSet):
    model = BorrowingItem
    serializer_class = BorrowingItemSerializer

    def get_queryset(self):
        return BorrowingItem.get_list_borrowing_item(
            code=self.kwargs.get("code"),
            status=self.request.GET.get("status"),
        )


class BorrowItems(generics.CreateAPIView):
    serializer_class = BorrowItemSerializer

    def get_queryset(self):
        return CartItem.get_list_cart_item(cart_code=self.kwargs.get("code"))
