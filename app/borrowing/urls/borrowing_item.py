from django.urls import path, include
from rest_framework.routers import DefaultRouter

from app.book_information.views import BookInformationList
from app.borrowing.views import BorrowingViews, BorrowingItemsViews

app_name = 'borrowing'
router = DefaultRouter()
router.register(r'', BorrowingItemsViews, basename='borrowing_item_views')

urlpatterns = [
    path('borrowing/<str:code>/items', include(router.urls)),
]