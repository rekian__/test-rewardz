from django.urls import path

from app.borrowing.views import BorrowItems

app_name = 'borrowing'

urlpatterns = [
    path('borrow/', BorrowItems.as_view(), name='borrow_item'),
]