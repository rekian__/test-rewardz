# Generated by Django 4.1.4 on 2022-12-31 01:39

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('book_information', '0002_bookinformation_available'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookinformation',
            name='will_available_at',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
