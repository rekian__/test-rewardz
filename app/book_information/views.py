from django.shortcuts import render
from rest_framework import generics

from app.book_information.models import BookInformation
from app.book_information.serializers import BookInformationSerializer


# Create your views here.
class BookInformationList(generics.ListAPIView):
    """ List """
    serializer_class = BookInformationSerializer

    def get_queryset(self):
        return BookInformation.get_list_book()
