import datetime

from rest_framework import serializers

from app.borrowing.models import BorrowingItem


class BorrowItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BorrowingItem
        fields = ("book", "borrowing")

    def create(self, validated_data):
        borrow_item = BorrowingItem()
        borrow_item.book = validated_data.get('book')
        borrow_item.borrowing = validated_data.get('borrowing')
        borrow_item.borrow_date = datetime.datetime.now()
        borrow_item.return_date = datetime.datetime.now() + datetime.timedelta(days=30)
        borrow_item.save()

        return borrow_item
