"""book_library URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/v1/book-information/', include("app.book_information.urls", namespace="book_information")),
    path('api/v1/', include('app.borrowing.urls.borrowing', namespace="borrowing")),
    path('api/v1/', include('app.borrowing.urls.borrowing_item', namespace="borrowing_items")),
    path('api/v1/', include('app.borrowing.urls.borrow', namespace="borrow")),
]
