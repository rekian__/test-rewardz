from django.urls import path

from app.book_information.views import BookInformationList

app_name = 'book'
urlpatterns = [
    path('', BookInformationList.as_view(), name='list_product'),
]