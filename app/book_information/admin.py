from django.contrib import admin

from app.book_information.models import BookInformation

# Register your models here.
admin.site.register(BookInformation)
