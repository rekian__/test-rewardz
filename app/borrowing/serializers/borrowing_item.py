from rest_framework import serializers

from app.book_information.models import BookInformation
from app.borrowing.models import Borrowing, BorrowingItem


class BorrowingItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = BorrowingItem
        fields = (
            "borrowing", "book", "borrow_date", "return_date", "is_extended", "status", "created_at", "edited_at",
        )
